# example-external-python

alias: https://onderzoek.amsterdam.nl/extern/example-external-python/

This file can only be rendered if [quarto.org](https://quarto.org/docs/getting-started/) is installed on your device.

---

After downloading quarto you can render the `.ipynb` file by running the following in the terminal:

`quarto render notebooks/index.ipynb`